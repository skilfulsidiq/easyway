import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trip',
  templateUrl: 'trip.html',
})
export class TripPage {
trip;
to;
from;
date;
time;
setTrip = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.trip = this.navParams.get("trip");
   console.log(this.trip);
    this.to = this.trip.to;
    this.from = this.trip.from;
    this.date = this.trip.date;
    this.time = this.trip.time;



   if(this.trip != '' || this.trip != null){
     this.setTrip = true;
   }else{
     this.setTrip = false;
   }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TripPage');
  }

}
