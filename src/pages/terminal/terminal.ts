import { Component, } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,
  LoadingController,
  Loading } from 'ionic-angular';
  import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";

/**
 * Generated class for the TerminalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terminal',
  templateUrl: 'terminal.html',
})
export class TerminalPage {

  pickup;
  destination;
  user;
  tripdate;
  loading:Loading;
  triptime;
  
    constructor(public navCtrl: NavController, public navParams: NavParams,
      public mobilefirst:MobilefirstProvider, public alert:AlertController, 
      public loadingCtrl:LoadingController) {
      this.pickup = this.navParams.get("pickUpTerminus");
      this.destination = this.navParams.get("destination");
      this.user=this.navParams.get("user");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TerminalPage');
  }

checkIn(){
  let TRAVEL_DATE = this.tripdate;
  let TRAVEL_TIME = this.triptime;
  let COMMUTER_ID = this.user.COMMUTER_ID;
  let FROM_DESTINATION_ID = this.pickup;
  let TO_DESTINATION_ID = this.destination;
  let trip = {from:this.pickup,to:this.destination,date:this.tripdate,time:this.triptime};
  let credential ="TRAVEL_DATE="+TRAVEL_DATE+"&TRAVEL_TIME="+TRAVEL_TIME+"&COMMUTER_ID="+COMMUTER_ID+"&FROM_DESTINATION_ID="+FROM_DESTINATION_ID+"&TO_DESTINATION_ID="+TO_DESTINATION_ID;
  this.loading =  this.loadingCtrl.create({content: "Please wait..."});
  this.loading.present();
  this.mobilefirst.createbooking(credential).subscribe(data=>{
      console.log(data);
    this.loading.dismissAll();
    this.navCtrl.setRoot("TripPage",{data:data,trip:trip });
  },error => {
    this.loading.dismissAll();
    console.log(error);
  });

}

}
