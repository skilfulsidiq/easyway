import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";

/**
 * Generated class for the DestinationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-destination',
  templateUrl: 'destination.html',
})
export class DestinationPage {
destination;
pickup;
terminal;
user;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public mobilefirst:MobilefirstProvider,  public view:ViewController,public modal:ModalController) {
    this.pickup = this.navParams.get("pickUpTerminus");
    this.terminal = this.navParams.get("destination");
    this.user=this.navParams.get("user");
    console.log(this.pickup +"mmm");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DestinationPage');
  }
  closeModal(){
    this.view.dismiss();
  }
  selectTerminus(terminusName){
      this.navCtrl.push("TripPage",{pickup:this.pickup,destination:terminusName,user:this.user});
    console.log(terminusName);
  }

}
