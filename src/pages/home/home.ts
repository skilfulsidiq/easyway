import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  LoadingController,
  Loading
} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {TERMINAL_LIST} from "../../mocks/terminal";
// import {BUS_LIST} from "../../mocks/bus";
import {MockProvider} from "../../providers/mock/mock";
import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";
import {Observable} from "rxjs/Observable";
// import {BusInterface} from "../../models/bus.interface";
// import {Observable} from "rxjs/Observable";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  terminal;
  buslist=[];
  bus;
  loading:Loading;
  user;
  firstname;
  lastname;
  commuter;
  TO;
  FROM;
  tripset = false;
  // objectKeys = Object.keys;
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation:Geolocation,
              public alert:AlertController,public modal:ModalController, public mobilefirst:MobilefirstProvider,
              public loadingCtrl:LoadingController) {
      this.user = this.navParams.get("User");
      this.commuter = this.navParams.get("user");
      // this.firstname = this.user.FIRSTNAME;
      // this.lastname = this.user.LASTNAME;
      this.FROM= this.navParams.get("pickup");
      this.TO= this.navParams.get("destination");

      console.log(this.user);
      this.allterminus();//show list of terminal in dropdown
      this.getLocation();//get user location

  }

  ionViewDidLoad() {
    if(this.FROM !='' && this.TO !=''){
      this.tripset = true;
    }
    console.log('ionViewDidLoad HomePage');
  }
  getLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  allterminus(){
    this.mobilefirst.getTerminus().subscribe(data =>{
     this.terminal = data;
    },err=>{
      console.log(err);
    });
  }
  showAlert(title, msg){
    let prompt = this.alert.create();
      prompt.setTitle(title);
      prompt.setMessage(msg);
      prompt.addInput({
          type:'date',
          name:'date',
          label:'Set trip date',
          value:"date",
      });
      prompt.addInput({
      type: 'time',
        name:'time',
      label: 'Set trip time',
      placeholder: 'HH:mm'
    });

    prompt.addButton('Cancel');
    prompt.addButton({
      text: 'Okay',
      handler: data => {
        this.createBooking(data.date,data.time);
        console.log('Checkbox data:', data);
        // this.testCheckboxOpen = false;
        // this.testCheckboxResult = data;
      }
    });
    prompt.present();

  }
  
  goTerminus(){
    let showmodal = this.modal.create("BusmodalPage",{pickupterminal:this.terminal,user:this.user});
    showmodal.present();
      // this.navCtrl.push("TerminalPage");

  }
  selectToTerminus(){
      // this.TO = terminus;
      this.tripset = true;
  }
  selectFromTerminus(terminus){
    this.FROM = terminus;
  }

  GoCheckIn(){
      this.showAlert("Check In", "Set your trip date and time");
  }
  createBooking(date,time){
    let TRAVEL_DATE = date;
    let TRAVEL_TIME = time;
    let COMMUTER_ID = this.commuter.COMMUTER_ID;
    let FROM_DESTINATION_ID = this.FROM;
    let TO_DESTINATION_ID = this.TO;
    let trip = {from:this.FROM,to:this.TO,date:date,time:time};
    let credential ="TRAVEL_DATE="+TRAVEL_DATE+"&TRAVEL_TIME="+TRAVEL_TIME+"&COMMUTER_ID="+COMMUTER_ID+"&FROM_DESTINATION_ID="+FROM_DESTINATION_ID+"&TO_DESTINATION_ID="+TO_DESTINATION_ID;
    this.loading =  this.loadingCtrl.create({content: "Please wait..."});
    this.loading.present();
    this.mobilefirst.createbooking(credential).subscribe(data=>{
        console.log(data);
      this.loading.dismissAll();
      this.navCtrl.setRoot("TripPage",{data:data,trip:trip });
    },error => {
      this.loading.dismissAll();
      console.log(error);
    })
  }
}
