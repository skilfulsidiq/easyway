import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController, ModalController,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';
import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";

/**
 * Generated class for the BusmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-busmodal',
  templateUrl: 'busmodal.html',
})
export class BusmodalPage {
terminal;
loading : Loading;
booking;
user;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public view:ViewController, public alertCtrl: AlertController,public mobilefirst:MobilefirstProvider,
              public loadingCtrl:LoadingController,public modal:ModalController) {
    this.terminal = this.navParams.get("pickupterminal");
    this.user=this.navParams.get("user");
    console.log(this.terminal);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusmodalPage');
  }
  closeModal(){
    this.view.dismiss();
  }
  //check bus status
  checkBusStatus(busId){
      let passeger = 20;
      // this.alert("There are stll "+passeger+" seats available in this bus, Do you want to check in? ");
  }

//  show alert
//   alert(message: string) {
    // this.alertCtrl.create({
    //   title: 'Confirm',
    //   subTitle: message,
    //   buttons: [{
    //     text: 'Check In',
    //     handler: () => {
    //       //yes function
    //     }
    //   },
    //     {
    //       text: 'Cancel',
    //       handler: () => {
    //         console.log('Disagree clicked');
    //       }
    //     }]
    // }).present();
  //   let showalert = this.alertCtrl.create();
  //   showalert.setTitle("Check In");
  //   showalert.setMessage(message);
  //   showalert.addInput({
  //     type: 'checkbox',
  //     label: 'Check In',
  //     value: 'yes',
  //     checked: false
  //   });
  //   showalert.addButton('Cancel');
  //   showalert.addButton({
  //     text:"Yes",
  //     handler: data => {
  //       console.log('Checkbox data:', data);
  //     }
  //   });
  //     showalert.present();
  // }

  // bookme(){
  //   let TRAVEL_DATE = "2018-04-30";
  //   let TRAVEL_TIME = "18:34:00";
  //   let COMMUTER_ID = this.booking.COMMUTER_ID;
  //   let FROM_DESTINATION_ID = "23";
  //   let TO_DESTINATION_ID = "10";
  //
  //   let credential ="TRAVEL_DATE="+TRAVEL_DATE+"&TRAVEL_TIME="+TRAVEL_TIME+"&COMMUTER_ID="+COMMUTER_ID+"&FROM_DESTINATION_ID="+FROM_DESTINATION_ID+"&TO_DESTINATION_ID="+TO_DESTINATION_ID;
  //   this.loading =  this.loadingCtrl.create({content: "Please wait..."});
  //   this.loading.present();
  //     this.mobilefirst.createbooking(credential).subscribe(data=>{
  //       this.loading.dismissAll();
  //       this.navCtrl.setRoot("TerminalPage",{data:data});
  //     },error => {
  //       this.loading.dismissAll();
  //         console.log(error);
  //     })
  // }
  selectTerminus(terminusName){
    let showmodal = this.modal.create("DestinationPage",{pickUpTerminus:terminusName,destination:this.terminal,user:this.user});
    showmodal.present();
    console.log(terminusName);
  }

}
