import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusmodalPage } from './busmodal';

@NgModule({
  declarations: [
    BusmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(BusmodalPage),
  ],
})
export class BusmodalPageModule {}
