// import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Loading,
  Toast,
  ToastController,
  AlertController
} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";
import {Device} from "@ionic-native/device";
import {Geolocation} from "@ionic-native/geolocation";
// import {Device} from "@ionic-native/device";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form;

  deviceInfo = { id: '',
    model: '',
    osname: '',
    version: '',
    manufacturer: '',
    serial: '',

  };
  long:number;
  lat:number;
  loading:Loading;
  toast: Toast;
  constructor(public navCtrl: NavController,private fireAuth:AngularFireAuth,
    public loadingCtrl:LoadingController,
              public toastCtrl:ToastController,
              public alertCtrl:AlertController,
              private mobilefirst:MobilefirstProvider,
              public device:Device,
              public geolocation:Geolocation

  ) {
      this.form = new FormGroup({
        email: new FormControl("",Validators.required),
        password: new FormControl("",Validators.required),
      });
      //device info
      this.deviceInfo.id = this.device.uuid;
      this.deviceInfo.model = this.device.model;
      this.deviceInfo.version = this.device.version;
    this.deviceInfo.manufacturer = this.device.manufacturer;
    this.deviceInfo.serial = this.device.serial;
    this.deviceInfo.osname = this.device.platform;
  //  geolocation
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      console.log(resp.coords.longitude,resp.coords.latitude);
      this.long = resp.coords.longitude;
      this.lat = resp.coords.latitude

      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }

  showAlert(alertTitle, alertMesg){
    let prompt = this.alertCtrl.create({
      title:alertTitle,
      message:alertMesg,
      buttons:[{text:'Ok'}]
    });
    prompt.present();
  }
  processLogin(){
      let xUSERNAME = this.form.value.email;
      let xPASSWORD = this.form.value.password;
      let OTHERINFO = this.lat+" ~ "+this.long+" ~ "+this.deviceInfo.id+" ~ "+"Ipaddress"+" ~ "+this.deviceInfo.model+" ~ "+
        this.deviceInfo.osname+" ~ "+this.deviceInfo.version+" ~ "+this.deviceInfo.manufacturer+" ~ "+this.deviceInfo.serial;
      console.log(OTHERINFO);
      if(xUSERNAME === ""||xPASSWORD === ""){
        this.showAlert("Login failure", "username and password are required" );
        return;
      }else{
        // console.log(xUSERNAME,xPASSWORD);
        // let credential ={xUSERNAME,xPASSWORD};
        let credential = "xUSERNAME="+xUSERNAME+"&xPASSWORD="+xPASSWORD;
        // let user = JSON.stringify(credential);

        // console.log(this.xUSERNAME, this.xPASSWORD);
        this.loading =  this.loadingCtrl.create({content: "Please wait..."}
        )
        this.loading.present();
        this.mobilefirst.login(credential).subscribe( res=>{
          this.loading.dismissAll();
          this.presentToast('Success! You\'re Logged in');
          this.navCtrl.setRoot("TabsPage",{User:res});
          console.log(res);
        },err=>{
          this.loading.dismissAll();
          console.log(err);
        });
        // this.showAlert("Login", "Signing-in as "+xUSERNAME);
      }
  }
  // // signin(){
  //   this.navCtrl.setRoot("TabsPage");
  // }
  signup(){
    this.navCtrl.push("RegisterPage");
  }
  signup2(){
    this.navCtrl.push("RegisterPage");
  }

  fblogin(){
   this.loading =  this.loadingCtrl.create({content: "Please wait..."});
    this.loading.present();
    this.fireAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then( res=>{
        this.loading.dismissAll();
        this.presentToast('Success! You\'re Logged in');
          this.navCtrl.setRoot("TabsPage",{User:res.user});
      console.log(res.user);
    },
    error =>{
      this.loading.dismissAll();
      console.log(error);
    }

    )
  }

  // login()

}
