import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams, Toast,
  ToastController
} from 'ionic-angular';
import {MobilefirstProvider} from "../../providers/mobilefirst/mobilefirst";
import {FormGroup,FormControl,Validators} from "@angular/forms";
import {TabsPage} from "../tabs/tabs";
import {Geolocation} from "@ionic-native/geolocation";
import {Device} from "@ionic-native/device";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  deviceInfo = { id: '',
    model: '',
    osname: '',
    version: '',
    manufacturer: '',
    serial: '',

  };

  long:number;
  lat:number;
  loading:Loading;
  toast: Toast;
  form;
  App_Type = "LAM";
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl:LoadingController,public toastCtrl:ToastController,
              public alertCtrl:AlertController, private mobilefirst:MobilefirstProvider,
              public device:Device,
              public geolocation:Geolocation) {
    //validate forms data
    this.form = new FormGroup({
      firstname: new FormControl("",Validators.required),
      lastname: new FormControl("",Validators.required),
      middlename: new FormControl("",Validators.required),
      phone: new FormControl("",Validators.required),
      email: new FormControl("",Validators.required),
      nextofkinphone: new FormControl("",Validators.required),
      username: new FormControl("",Validators.required),
      password: new FormControl("",Validators.required)
    });
    //device info
    this.deviceInfo.id = this.device.uuid;
    this.deviceInfo.model = this.device.model;
    this.deviceInfo.version = this.device.version;
    this.deviceInfo.manufacturer = this.device.manufacturer;
    this.deviceInfo.serial = this.device.serial;
    this.deviceInfo.osname = this.device.platform;
    //  geolocation
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      console.log(resp.coords.longitude,resp.coords.latitude);
      this.long = resp.coords.longitude;
      this.lat = resp.coords.latitude
      if(this.long == null){
        this.long = 0.00;
      }
      if(this.lat == null){
        this.lat = 0.00;
      }

      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    if(this.deviceInfo.id == null ){
        this.deviceInfo.id = "20.2";
    }
    if(this.deviceInfo.manufacturer == null ){
      this.deviceInfo.manufacturer = "20.3";
    }
    if(this.deviceInfo.model == null ){
      this.deviceInfo.model = "20.4";
    }
    if(this.deviceInfo.osname == null ){
      this.deviceInfo.osname = "20.5";
    }
    if(this.deviceInfo.serial == null ){
      this.deviceInfo.serial = "20.6";
    }
    if(this.deviceInfo.version == null ){
      this.deviceInfo.version = "20.7";
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }

  showAlert(alertTitle, alertMesg){
    let prompt = this.alertCtrl.create({
      title:alertTitle,
      message:alertMesg,
      buttons:[{text:'Ok'}]
    });
    prompt.present();
  }
  ProcessRegister(){
      let FIRSTNAME = this.form.value.firstname;
      let LASTNAME= this.form.value.lastname;
    let MIDDLENAME= this.form.value.middlename;
    let PHONE_NUMBER= this.form.value.phone;
    let EMAILADDRESS= this.form.value.email;
    let PHONE_NUMBER_NEXTOFKIN= this.form.value.nextofkinphone;
    let USERNAME= this.form.value.username;
    let PASSWORD= this.form.value.password;
    let LOGINMETHOD = "EMAIL";
    let APP_TYPE = this.App_Type;
    let STREET1  ="";
    let STREET2 = "";
    let LOCATION = "";
    let LGA = "";
    let STATE = "";
    let COUNTRY = "";
     let OTHERDETAILS = this.lat+"~"+this.long+"~"+this.deviceInfo.id+"~"+"3.0"+"~"+this.deviceInfo.model+"~"+
      this.deviceInfo.osname+"~"+this.deviceInfo.version+"~"+this.deviceInfo.manufacturer+"~"+this.deviceInfo.serial;

    console.log(OTHERDETAILS);
      // if(email == "" || password === ""){
      //   this.showAlert("Registration fails", "Email and password are required" );
      // }else{
      //   let credential = {LASTNAME,FIRSTNAME,MIDDLENAME,PHONE_NUMBER,EMAIL_ADDRESS,
      //     LOGINMETHOD,STREET1,STREET2,LOCATION,LGA,STATE,COUNTRY,PHONE_NUMBER_NEXTOFKIN,USERNAME,PASSWORD,OTHERDETAILS,APP_TYPE};

    let credential = "LASTNAME="+LASTNAME+"&FIRSTNAME="+FIRSTNAME+"&MIDDLENAME="+MIDDLENAME+"&PHONE_NUMBER="+PHONE_NUMBER
    +"&EMAILADDRESS="+EMAILADDRESS+"&LOGINMETHOD="+LOGINMETHOD+"&STREET1="+STREET1+"&STREET2="+STREET2+"&LOCATION="+LOCATION+"&LGA="+LGA
    +"&STATE="+STATE+"&COUNTRY="+COUNTRY+"&PHONE_NUMBER_NEXTOFKIN="+PHONE_NUMBER_NEXTOFKIN+"&USERNAME="+USERNAME+"&PASSWORD="+PASSWORD
    +"&OTHERDETAILS="+OTHERDETAILS+"&APP_TYPE="+APP_TYPE;

        this.loading =  this.loadingCtrl.create({content: "Please wait..."});
        this.loading.present();
        this.mobilefirst.register(credential).subscribe( res=>{
          this.loading.dismissAll();
          // let me = res;
          // if( == 0){
            this.navCtrl.setRoot("TabsPage",{User:res});
            this.presentToast('Success! You\'re Logged in');
          // }else{
            this.showAlert("Registration fails", "Email and password are required");
          // }
          console.log(res);
        },err=>{
          this.loading.dismissAll();
          console.log(err);
        });
        // this.showAlert("Login", "Signing-in as "+email);

    }


}
