import { Component } from '@angular/core';
import {IonicPage, NavParams,} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

    tabRoot1 : string;
    tabRoot2 : string;
    tabRoot3 : string;
    user;
  constructor(public navParams:NavParams,public geolocation:Geolocation) {
  
    this.tabRoot1 = "HomePage";
    this.tabRoot2 = "TripPage";
    this.tabRoot3 = "MapPage";
    this.user = this.navParams.get("User");
  }

  getLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
      // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
}
