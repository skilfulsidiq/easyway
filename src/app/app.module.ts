import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { MyApp } from './app.component';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Geolocation} from "@ionic-native/geolocation";
import { MobilefirstProvider } from '../providers/mobilefirst/mobilefirst';
// import {HttpModule} from "@angular/http";
import {HttpClientModule} from "@angular/common/http";
import { MockProvider } from '../providers/mock/mock';
import {Device} from "@ionic-native/device";
var config = {
  apiKey: "AIzaSyDV5DCl8LDnSVEGtg7NtiYBIvrUcyQmscs",
  authDomain: "easyway-5d90a.firebaseapp.com",
  databaseURL: "https://easyway-5d90a.firebaseio.com",
  projectId: "easyway-5d90a",
  storageBucket: "easyway-5d90a.appspot.com",
  messagingSenderId: "977473301320"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},Geolocation,
    MobilefirstProvider,HttpClientModule,
    MockProvider,
    Device
  ]
})
export class AppModule {}
