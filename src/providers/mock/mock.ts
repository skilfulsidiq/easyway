import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BUS_LIST} from "../../mocks/bus";

/*
  Generated class for the MockProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MockProvider {
bus = BUS_LIST;
thebus:any = [];
  constructor(public http: HttpClient) {
    console.log('Hello MockProvider Provider');
  }

  getBus(driver){
   let goingbus = this.bus.filter(x=>x.terminus == driver);
   // goingbus.forEach((driver)=>{
   //   this.thebus.push(driver);
   // })
   return goingbus;
  }


}
