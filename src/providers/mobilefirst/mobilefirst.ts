/// <reference path="../../../plugins/cordova-plugin-mfp/typings/worklight.d.ts" />
import { Injectable } from '@angular/core';
import { Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {HttpClient,HttpHeaders} from "@angular/common/http";
// import {Http} from "@ionic-native/core";

/*
  Generated class for the MobilefirstProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// var headers = new HttpHeaders();
// headers.append('Access-Control-Allow-Origin' , '*');
// headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
// headers.append("Accept","application/json");
// headers.append("Content-Type", 'application/json');
// headers.set('Content-Type', 'application/x-www-form-urlencoded');
let mobilefirstUrl = "https://mobilefoundation-8n17-server.eu-gb.mybluemix.net/mfp/api/adapters/Lamata_Adapter/";
let mobilefirstReg="https://mobilefoundation-8n17-server.eu-gb.mybluemix.net/mfp/api/adapters/Lamata_Adapter/createusers/{LASTNAME}/{xPASSWORD}/{MIDDLENAME}/{PHONE_NUMBER}/{EMAILADDRESS}/{LOGINMETHOD}/{STREET1}/{STREET2}/{LOCATION}/{LGA}/{STATE}/{COUNTRY}/{PHONE_NUMBER_NEXTOFKIN}/{USERNAME}/{PASSWORD}/{OTHERDETAILS}/{APP_TYPE}";
// let option = new RequestOptions({headers:headers});
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded;charset=UTF-8'
  })
}
@Injectable()
export class MobilefirstProvider {
  data : any = null
  constructor(public http:HttpClient) {
    console.log('Hello MobilefirstProvider Provider');
  }
  //https://mobilefoundation-8n17-server.eu-gb.mybluemix.net/mfp/api/adapters/Lamata_Adapter/userlogon/{xUSERNAME}/{xPASSWORD}
  // login(credential) {
  //   console.log('--> Mobilefirst loading data from adapter ...');
  //   return new Promise((resolve, reject)  => {
  //     if (this.data) {
  //       // already loaded data
  //       return resolve(this.data);
  //     }
  //     // don't have the data yet
  //     var url = "/mfp/api/adapters/Lamata_Adapter/userlogon/"+credential;
  //     let dataRequest = new WLResourceRequest(url, WLResourceRequest.POST);
  //     console.log("am in data request");
  //     dataRequest.send().then(
  //       (response) => {
  //         console.log('--> Mobilefirst loading data from adapterr\n', response);
  //         this.data = response.responseJSON;
  //         resolve(this.data);
  //       }, (failure) => {
  //         console.log('--> MyProvider failed to load data\n', JSON.stringify(failure));
  //         reject(failure);
  //       })
  //   });
  // }
  login(credential){
    // var url = "/mfp/api/adapters/Lamata_Adapter/userlogon/"+xUSERNAME+"/"+xPASSWORD;
    console.log(JSON.stringify(credential));
    return this.http.post(mobilefirstUrl+"userlogon/{xUSERNAME}/{xPASSWORD}",credential,httpOptions);
      // .map(res => res.json());

  }
  register(credential){
      console.log(credential);
    return this.http.post(mobilefirstReg,credential,httpOptions);
  }
  createbooking(credential){
    return this.http.post(mobilefirstUrl+"createbooking/{TRAVEL_DATE}/{TRAVEL_TIME/{COMMUTER_ID}/{FROM_DESTINATION_ID}/{TO_DESTINATION_ID}",credential,httpOptions);
  }
  getTerminus(){
    return this.http.get(mobilefirstUrl+"allterminals",httpOptions);
  }
}
