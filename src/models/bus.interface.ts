export interface BusInterface {
      id:number;
      number:number;
      destination:string;
      terminus:string;
      driver:string;
}
