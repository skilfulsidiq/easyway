import {Terminal} from "../models/terminal.interface";

const terminaList: Terminal[] = [
  {id:1,title:"TBS", long:234567,lat:78654},
  {id:2,title:"Barrack", long:234567,lat:78654},
  {id:3,title:"Fadeyi", long:234567,lat:78654},
  {id:4,title:"Mile12", long:234567,lat:78654},
  {id:5,title:"Ikorodu", long:234567,lat:78654}
];
export const TERMINAL_LIST = terminaList;
