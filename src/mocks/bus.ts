import {BusInterface} from "../models/bus.interface";

const buslist: BusInterface[] = [
  {id:1, number:1, destination:"Fadeyi", terminus:"TBS",driver:"Ade"},
  {id:2, number:2, destination:"Mile 12", terminus:"TBS",driver:"Ade"},
  {id:3, number:3, destination:"ARUNA", terminus:"IKORODU",driver:"Ade"},
  {id:4, number:4, destination:"OWODE", terminus:"ARUNA",driver:"Ade"},
  {id:5, number:5, destination:"IKORODU", terminus:"IKORODU",driver:"Ade"},
  {id:6, number:6, destination:"CMS", terminus:"BARRACK",driver:"Ade"},
  {id:7, number:7, destination:"MARYLAND", terminus:"IKORODU",driver:"Ade"},
  {id:8, number:8, destination:"ONIPANU", terminus:"TBS",driver:"Ade"}

];
export const BUS_LIST = buslist;
